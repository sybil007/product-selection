import { NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ReactiveFormsModule} from '@angular/forms';
import {SelectProductControlComponent} from './components/select-product-control/select-product-control.component';

@NgModule({
  declarations: [
    AppComponent,
    SelectProductControlComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
