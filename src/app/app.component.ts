import { Component } from '@angular/core';
import { FormControl, FormGroup} from '@angular/forms';
import {ProductModel} from './models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  form = new FormGroup({
    productId: new FormControl(null)
  });

  products: ProductModel[] = [
    {id: 1, description: 'Description of product 1', name: 'Product 1'},
    {id: 2, description: 'Description of product 2', name: 'Product 2'},
    {id: 3, description: 'Description of product 3', name: 'Product 3'},
    {id: 4, description: 'Description of product 4', name: 'Product 4'},
    {id: 5, description: 'Description of product 5', name: 'Product 5'},
    {id: 6, description: 'Description of product 6', name: 'Product 6'},
  ];

  onSubmit() {
    console.log(this.form.value);
    alert(this.form.value.productId ? `Product id ${this.form.value.productId}` : "No product selected");
  }
}
