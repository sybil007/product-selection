import {Component, Input} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {ProductModel} from '../../models';

@Component({
  selector: 'app-select-product-control',
  templateUrl: './select-product-control.component.html',
  styleUrls: ['./select-product-control.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: SelectProductControlComponent,
      multi: true
    }
  ],
})
export class SelectProductControlComponent implements ControlValueAccessor {
  showAccordion: boolean = false;
  selectedId: number | null= null;

  @Input() disabled = false;
  @Input() products: ProductModel[] = [];

  onChange =(id: number | null) => {};
  onTouched = () => {};

  selectProduct(id: number) {
    if (!this.disabled) {
      this.writeValue(id);
    }
  }

  writeValue(id: number): void {
    if (this.selectedId === id) {
      this.selectedId = null;
    } else {
      this.selectedId = id;
    }
    this.onChange(this.selectedId);
  }

  registerOnChange(fn: (id: number | null) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
