import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SelectProductControlComponent } from './select-product-control.component';
import {By} from '@angular/platform-browser';

describe('RatingInputComponent', () => {
  let component: SelectProductControlComponent;
  let fixture: ComponentFixture<SelectProductControlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectProductControlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectProductControlComponent);
    component = fixture.componentInstance;

    component.products = [
      {id: 1, description: 'Description of product 1', name: 'Product 1'},
      {id: 2, description: 'Description of product 2', name: 'Product 2'},
      {id: 3, description: 'Description of product 3', name: 'Product 3'}
    ];
    fixture.detectChanges();
  });

  it('Should create', () => {
    expect(component).toBeTruthy();
  });

  it('Accordion should collapsed on init', () => {
    const element = fixture.debugElement.query(By.css('.products-container'));
    expect(element).toBeNull();
  });

  it('Accordion should be expanded after click', () => {
    const arrow = fixture.debugElement.query(By.css('.arrow'));
    arrow.nativeElement.click();
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('.products-container'));
    expect(element).not.toBeNull();
  });

  it('Products list should contain any element', () => {
    component.showAccordion = true;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('.product-item'));
    expect(element).not.toBeNull();
  });

  it('Id should be changed after selecting product', () => {
    component.showAccordion = true;
    fixture.detectChanges();

    let element = fixture.debugElement.query(By.css('#product-2'));
    element.nativeElement.click();
    fixture.detectChanges();

    expect(component.selectedId).toEqual(2);
  });

  it('Selected product should be highlighted', () => {
    component.showAccordion = true;
    component.selectedId = 2;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('#product-2'));
    expect(element.classes["selected-product"]).toEqual(true)
  });

  it('Click on current product should unselect it', () => {
    component.showAccordion = true;
    component.selectedId = 2;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('#product-2'));
    element.nativeElement.click();
    fixture.detectChanges();

    expect(component.selectedId).toBeNull();
  });

  it('Product should be still selected after collapsing and expanding accordion', () => {
    const arrow = fixture.debugElement.query(By.css('.arrow'));
    arrow.nativeElement.click();
    fixture.detectChanges();

    let element = fixture.debugElement.query(By.css('#product-2'));
    element.nativeElement.click();
    fixture.detectChanges();

    // Collapse accordion
    arrow.nativeElement.click();
    fixture.detectChanges();
    // Expand accordion again
    arrow.nativeElement.click();
    fixture.detectChanges();

    element = fixture.debugElement.query(By.css('#product-2'));
    expect(element.classes["selected-product"]).toEqual(true)
  });

});
